jamulus (3.8.2+dfsg-1) unstable; urgency=low

  * New upstream release
  * Revert bad upstream change to where --help output is printed
  * Use manpage contributed upstream

 -- Thorsten Glaser <tg@mirbsd.de>  Fri, 25 Feb 2022 19:52:53 +0100

jamulus (3.8.1+dfsg1-1) unstable; urgency=low

  * Update list of directories to exclude
  * New upstream release
  * Update manpage and other related files and patches
  * No longer jdupes, the homepage contents are no longer shipped
  * Policy 4.6.0.1 (no relevant changes)
  * Fix disabling the update checker
  * Redo lintian overrides

 -- Thorsten Glaser <tg@mirbsd.de>  Thu, 23 Dec 2021 21:24:00 +0100

jamulus (3.6.2+dfsg1-3) unstable; urgency=low

  * Update GitHub URLs
    - the repository moved to a{n, new} organisation
    - GitHub’s tags and releases pages changed tarball links
  * Mark diff as applied upstream
  * Run jdupes on the installed documentation as lintian demands
  * Ship a manpage (Debian-specific for now)
  * Merge updated list of central (genre) servers offered by upstream
  * Also apply upstream fix for memory leaks
  * Add patch to correctly escape messages received from clients
  * Backport code around --serverpublicip to support running servers
    behind IPv4 NAT; also a prerequisite of the following patch
  * Privacy fix: cease sending a packet to Cloudflare to determine
    the (nōn-NAT) external interface / IP address

 -- Thorsten Glaser <tg@mirbsd.de>  Sun, 11 Apr 2021 16:04:40 +0200

jamulus (3.6.2+dfsg1-2) unstable; urgency=low

  * Upload to unstable

 -- Thorsten Glaser <tg@mirbsd.de>  Tue, 26 Jan 2021 00:03:00 +0100

jamulus (3.6.2+dfsg1-1) experimental; urgency=low

  * Initial packaging (Closes: #958146)

 -- Thorsten Glaser <tg@mirbsd.de>  Tue, 29 Dec 2020 23:11:37 +0100
